#! /bin/bash

# Resource updater for stuff not made by me.
cd debs && mkdir temp && cd temp

# PojavLauncher, by PojavLauncherTeam and DuyKhanhTran
wget 'https://nightly.link/PojavLauncherTeam/PojavLauncher_iOS/workflows/ios/main/PojavLauncher%20deb.zip'
unzip 'PojavLauncher deb.zip'
dpkg-deb --raw-extract pojavlauncher_iphoneos-arm.deb pojavlauncher_iphoneos-arm
rm -rfv pojavlauncher_iphoneos-arm.deb
echo "Depiction: https://doregon.github.io/cydia/depictions/?p=pojavlauncher" >> pojavlauncher_iphoneos-arm/DEBIAN/control
dpkg-deb -b pojavlauncher_iphoneos-arm
mv pojavlauncher_iphoneos-arm.deb ../pojavlauncher_iphoneos-arm.deb

# Space for others, if and when the time comes.

# Wrap up
cd ..
rm -rf temp
cd ..
rm -rf Packages*
dpkg-scanpackages debs /dev/null > Packages
bzip2 Packages
dpkg-scanpackages debs /dev/null > Packages
